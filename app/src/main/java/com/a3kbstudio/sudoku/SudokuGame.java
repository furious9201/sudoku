package com.a3kbstudio.sudoku;

import android.app.Activity;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

/**
 * Created by Kostya on 29.05.2017.
 */

public class SudokuGame extends Activity {

    private int last_row = -1;
    private int last_column = -1;
    private final int SIZE = 9;
    TextView[][] cell = new TextView[SIZE][SIZE];
    Button[] numbersButtons = new Button[SIZE];
    Button removeButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.sudoku_game);

        initSudokuTable();

        initNumbersButtons();

        initOtherButtons();

    }

    private void initOtherButtons() {
        removeButton = (Button) findViewById(R.id.removeButton);
        removeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cell[last_row][last_column].setText("");
            }
        });
    }

    public void resetTableColors() {
        for (int i = 0; i < SIZE; i++)
            for (int j = 0; j < SIZE; j++)
                cell[i][j].setBackgroundColor(getResources().getColor(R.color.white));
    }

    public void initSudokuTable(){
        TableLayout sudokuGrid = (TableLayout) findViewById(R.id.table_layout_id);
        Sudoku sudoku = new Sudoku();
        TableRow[] row = new TableRow[SIZE];

        for (int i = 0; i < SIZE; i++) {
            row[i] = new TableRow(this);
            TableLayout.LayoutParams tableRowParams =
                    new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.MATCH_PARENT, 1f);
            int leftMargin = 4;
            int rightMargin = 4;
            int bottomMargin;
            int topMargin = 0;
            if (i == 0)
                topMargin = 5;
            if (i == 2 || i == 5 || i == 8)
                bottomMargin = 5;
            else bottomMargin = 2;
            tableRowParams.setMargins(leftMargin, topMargin, rightMargin, bottomMargin);
            row[i].setLayoutParams(tableRowParams);
            for (int j = 0; j < SIZE; j++) {
                TableRow.LayoutParams llp = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.MATCH_PARENT, 1f);
                if (j == 0)
                    llp.setMargins(1, 0, 2, 0);
                else if (j == 8)
                    llp.setMargins(0, 0, 1, 0);
                else if (j == 2 || j == 5)
                    llp.setMargins(0, 0, 5, 0);
                else
                    llp.setMargins(0, 0, 2, 0);

                cell[i][j] = new TextView(this);
                cell[i][j].setBackgroundColor(getResources().getColor(R.color.white));
                cell[i][j].setLayoutParams(llp);
                int value = sudoku.getValueOfCell(i, j);
                String text = "";
                if (value != 0) {
                    text = Integer.toString(value);
                    cell[i][j].setTextColor(getResources().getColor(R.color.fillcell));
                }
                else
                    cell[i][j].setTextColor(getResources().getColor(R.color.emptycell));
                cell[i][j].setText(text);
                cell[i][j].setGravity(Gravity.CENTER);
                final int rows = i;
                final int col = j;
                cell[i][j].setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        resetTableColors();
                        if ((last_column == col) && (last_row == rows))
                        {
                            last_column = -1;
                            last_row = -1;
                            return;
                        }
                        last_column = col;
                        last_row = rows;
                        TextView textview = (TextView) view;
                        boolean forSetEnabledButtonsFlag;
                        if (textview.getCurrentTextColor() == getResources().getColor(R.color.fillcell))
                            forSetEnabledButtonsFlag = false;
                        else
                            forSetEnabledButtonsFlag = true;

                        for (int i = 0; i < SIZE; i++) {
                            cell[i][col].setBackgroundColor(getResources().getColor(R.color.rowcolblue));
                            cell[rows][i].setBackgroundColor(getResources().getColor(R.color.rowcolblue));
                            numbersButtons[i].setEnabled(forSetEnabledButtonsFlag);
                            removeButton.setEnabled(forSetEnabledButtonsFlag);
                        }
                        textview.setBackgroundColor(getResources().getColor(R.color.blue));
                    }
                });
                row[i].addView(cell[i][j]);
            }
            sudokuGrid.addView(row[i]);
        }
    }

    void initNumbersButtons(){
        numbersButtons[0] = (Button)findViewById(R.id.button1);
        numbersButtons[1] = (Button)findViewById(R.id.button2);
        numbersButtons[2] = (Button)findViewById(R.id.button3);
        numbersButtons[3] = (Button)findViewById(R.id.button4);
        numbersButtons[4] = (Button)findViewById(R.id.button5);
        numbersButtons[5] = (Button)findViewById(R.id.button6);
        numbersButtons[6] = (Button)findViewById(R.id.button7);
        numbersButtons[7] = (Button)findViewById(R.id.button8);
        numbersButtons[8] = (Button)findViewById(R.id.button9);

        for (int i = 0; i < SIZE; i++) {
            final int finalI = i;
            numbersButtons[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if ((last_column != -1) && (last_row != -1))
                        cell[last_row][last_column].setText(Integer.toString(finalI + 1));
                }
            });
        }
    }
}
