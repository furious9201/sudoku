package com.a3kbstudio.sudoku;

import android.widget.TextView;

import java.util.Random;

/**
 * Created by Kostya on 30.05.2017.
 */

public class Sudoku {

    private Random random = new Random();
    private final int SIZE = 9;
    private int[][] values = new int[SIZE][SIZE];

    public Sudoku() {
        for (int i = 0; i < SIZE; i++)
            for (int j = 0; j < SIZE; j++) {
                values[i][j] = ((i / 3) + 3 * (i % 3) + j % 9) % 9 + 1;
            }

        int amt = random.nextInt(10) + 101;

        mix(amt);

        makeSudokuWithLevel(30);
    }

    private void mix(int count) {
        for (int i = 0; i < count; i++) {
            int function = random.nextInt(5);
            switch (function) {
                case 1:
                    transporting();
                    break;
                case 2:
                    swap_colums_small();
                    break;
                case 3:
                    swap_rows_small();
                    break;
                case 4:
                    swap_colums_area();
                    break;
                default:
                    swap_rows_area();
                    break;
            }
        }
    }

    public int getValueOfCell(int row, int column) {
        return values[row][column];
    }

    public void makeSudokuWithLevel(int level) {
        int[][] cellLooks = new int[SIZE][SIZE];
        int iterator = 0;
        int limit = SIZE * SIZE;
        int difficult = limit;

        int [][]solution = new int[SIZE][SIZE];
        for (int row = 0; row < SIZE; row++)
            for (int col = 0; col < SIZE; col++)
                solution[row][col] = values[row][col];

        while ((iterator < limit) && (difficult > level)) {
            int i = random.nextInt(9);
            int j = random.nextInt(9);

            if (cellLooks[i][j] == 0) {
                iterator++;
                cellLooks[i][j] = 1;
                int temp = values[i][j];
                values[i][j] = 0;
                solution[i][j] = 0;
                difficult --;


                SudokuSolver solver = new SudokuSolver(solution);
                boolean solve = solver.solve();
                if (!solve)
                {
                    values[i][j] = temp;
                    solution[i][j] = temp;
                    difficult ++;
                }
            }
        }
    }

    private void swap_rows_small() {
        int area = random.nextInt(3);
        int row = random.nextInt(3);

        int rowNumber1 = area * 3 + row;
        int row2 = random.nextInt(3);
        while (rowNumber1 == row2)
            row2 = random.nextInt(3);
        int rowNumber2 = area * 3 + row2;

        for (int i = 0; i < 9; i++) {
            int temp = values[rowNumber1][i];
            values[rowNumber1][i] = values[rowNumber2][i];
            values[rowNumber2][i] = temp;
        }
    }

    private void swap_colums_small() {
        int area = random.nextInt(3);
        int column = random.nextInt(3);

        int colNumber1 = area * 3 + column;
        int column2 = random.nextInt(3);
        while (colNumber1 == column2)
            column2 = random.nextInt(3);
        int colNumber2 = area * 3 + column2;

        for (int i = 0; i < 9; i++) {
            int temp = values[i][colNumber1];
            values[i][colNumber1] = values[i][colNumber2];
            values[i][colNumber2] = temp;
        }
    }

    private void swap_rows_area() {
        int area1 = random.nextInt(3);
        int area2 = random.nextInt(3);
        while (area1 == area2)
            area2 = random.nextInt(3);

        for (int i = 0; i < 3; i++)
            for (int j = 0; j < SIZE; j++) {
                int temp = values[area1 * 3 + i][j];
                values[area1 * 3 + i][j] = values[area2 * 3 + i][j];
                values[area2 * 3 + i][j] = temp;

            }
    }

    private void swap_colums_area() {
        int area1 = random.nextInt(3);
        int area2 = random.nextInt(3);
        while (area1 == area2)
            area2 = random.nextInt(3);

        for (int i = 0; i < SIZE; i++)
            for (int j = 0; j < 3; j++) {
                int temp = values[i][area1 * 3 + j];
                values[i][area1 * 3 + j] = values[i][area2 * 3 + j];
                values[i][area2 * 3 + j] = temp;

            }
    }

    private void transporting() {
        for (int i = 0; i < SIZE; i++) {
            for (int j = i; j < SIZE; j++) {
                int temp = values[j][i];
                values[j][i] = values[i][j];
                values[i][j] = temp;
            }
        }
    }


}
